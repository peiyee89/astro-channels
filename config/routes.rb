Rails.application.routes.draw do
  root 'welcome#index'
  resources :users
  resources :sessions
  resources :user_favorite_channels

  get "/tv_guide", to: "welcome#tv_guide", as: "tv_guide"
  get "/next_page", to: "welcome#next_page", as: "next_page"


  match 'auth/:provider/callback', to: 'authentications#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
end
