class CreateAuthentications < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
      t.string :provider
      t.text :auth_hash
      t.text :access_token
      t.string :uid
      t.belongs_to :user
      t.timestamps null: false
    end
  end
end
