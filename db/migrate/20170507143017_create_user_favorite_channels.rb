class CreateUserFavoriteChannels < ActiveRecord::Migration
  def change
    create_table :user_favorite_channels do |t|
      t.integer :channel_id
      t.belongs_to :user
      t.timestamps null: false
    end
  end
end
