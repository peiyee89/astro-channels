# Release Notes Version 0.1.0 #

### Features

1. Allow users sign up with their email or continue with their Facebook account.
1. Allow signed in user to like/dislike any channels by pressing the heart icon.
1. Allow user to sort the channel list by channel title or channel number.
1. Allow user to view current showing channel program.

### Framework/Tools

* Rails 4.2.8
* Ruby 2.3.1
* Postgresql 9.5 or above

### How do I get set up? ###
Installing dependencies (all required libraries are in Gemfile)

```
$ bundle install
$ rake db:drop db:create db:schema:load db:migrate
```

Database configuration
```
$ rake db:drop db:create db:schema:load db:migrate
```

How to start rails server in localhost
```
$ rails s
```

How to run tests
```
$ rake db:drop db:create db:schema:load db:migrate db:test:prepare (for first time)
$ rspec spec
```

Deployment instructions
```
$ cap production deploy
```
[http://ec2-54-255-165-227.ap-southeast-1.compute.amazonaws.com/](http://ec2-54-255-165-227.ap-southeast-1.compute.amazonaws.com/)