require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:user) { create(:user) }
  let(:params) do
    {

    }
  end
  describe 'POST #create' do
    context 'when user is registered' do
      before do
        user
        params = {
          user: {
            email: user.email,
            name: user.name,
            password: "123",
          },
        }
        post :create, params
      end
      it { should set_flash }
      it { expect(flash[:notice]).to eq "Looks like you have already claimed your account. Please log in to continue." }
    end
    context 'when new user' do
      before do
        user
        params = {
          user: {
            email: Faker::Internet.email,
            name: Faker::Name.name,
            password: "123",
          },
        }
        post :create, params
      end
      it { should_not set_flash }
    end
  end
end
