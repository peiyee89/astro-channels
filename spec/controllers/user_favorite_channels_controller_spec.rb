require 'rails_helper'

RSpec.describe UserFavoriteChannelsController, type: :controller do
  let(:user) { create(:user) }
  describe 'POST #create' do
    context "when user logged in" do
      before do
        allow_any_instance_of(UserFavoriteChannelsController).to receive(:current_user).and_return(user)
        post :create, channel_id: 1, format: :js
        user.reload
      end
      it { expect(user.user_favorite_channels.count).to eq 1 }
    end
    context "when user not logged in" do
      before { post :create, channel_id: 1 }
      it { should redirect_to(new_session_path) }
    end
  end
end
