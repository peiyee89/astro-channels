require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  let(:email) { Faker::Internet.email }
  let(:password) { "123" }
  let(:user) { create(:user, email: email) }
  let(:params) do
    {
      session: {
        email: email,
        password: password,
      },
    }
  end
  describe 'POST #create' do
    context "when user exist and enter correct password" do
      before do
        user.update(password: password)
        post :create, params
      end
      it { should redirect_to(root_path) }
    end
    context "when user exist and enter wrong password" do
      before do
        user.update(password: "wrong")
        post :create, params
      end
      it { should set_flash }
      it { expect(flash[:notice]).to eq "Looks like you have enter wrong email or password." }
      it { should redirect_to(new_session_path) }
    end
    context "when user does not exist" do
      before do
        post :create, params
      end
      it { should set_flash }
      it { expect(flash[:notice]).to eq "Looks like you have enter wrong email or password." }
      it { should redirect_to(new_session_path) }
    end
  end
end
