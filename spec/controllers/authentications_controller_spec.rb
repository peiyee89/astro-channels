require 'rails_helper'

RSpec.describe AuthenticationsController, type: :controller do
  let(:user) { create(:user) }
  let(:provider) { "facebook" }
  let(:request) { -> { get(:create, provider: provider) } }
  let(:auth) do
    {
      name: user.name,
      email: user.email,
      provider: provider,
      uid: "123",
      access_token: "ABC",
    }
  end
  describe 'GET #create' do
    context "when user sign up with facebook" do
      before do
        new_auth = {
          name: Faker::Name.name,
          email: Faker::Internet.email,
          provider: provider,
          uid: "123",
          access_token: "ABC",
        }
        allow(AuthHelper).to receive(:new).and_return(Hashie::Mash.new(new_auth))
        request.call
      end
      it { should redirect_to(root_path) }
      it { expect(Authentication.all.count).to eq 1 }
    end

    context "when current user connect with facebook" do
      before do
        allow(AuthHelper).to receive(:new).and_return(Hashie::Mash.new(auth))
        allow_any_instance_of(AuthenticationsController).to receive(:current_user).and_return(user)
        request.call
      end
      it { should redirect_to(root_path) }
      it { expect(Authentication.all.count).to eq 1 }
    end

    context "when current user connected with facebook" do
      before do
        allow(AuthHelper).to receive(:new).and_return(Hashie::Mash.new(auth))
        create(:authentication, user: user)
        allow_any_instance_of(AuthenticationsController).to receive(:current_user).and_return(user)
        request.call
      end
      it { expect(response.body).to be_blank }
    end
  end
end
