require 'rails_helper'

RSpec.describe User, type: :model do
  it { should have_many(:authentications) }
  it { should have_many(:user_favorite_channels) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:name) }
end
