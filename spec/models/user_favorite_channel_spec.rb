require 'rails_helper'

RSpec.describe UserFavoriteChannel, type: :model do
  it { should belong_to(:user) }
  it { should validate_uniqueness_of(:channel_id).scoped_to(:user_id) }
end
