FactoryGirl.define do
  factory :authentication do
    user
    provider "facebook"
    uid "123"
  end
end
