require 'rails_helper'

RSpec.describe UrlUtility do
  let(:extended_module) { Module.new { extend UrlUtility } }
  describe '#query_string_builder' do
    it { expect(extended_module.query_string_builder(channel_id: 1)).to eq "?channel_id=1" }
    it { expect(extended_module.query_string_builder({})).to eq "" }
    it { expect(extended_module.query_string_builder(channel_id: "1,2", event_id: 2)).to eq "?channel_id=1,2&event_id=2" }
  end
end
