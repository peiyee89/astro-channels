require 'rails_helper'

RSpec.describe DurationParser do
  let(:valid_input1) { "00:00:10" }
  let(:valid_input2) { "00:20:10" }
  let(:valid_input3) { "01:20:10" }
  let(:valid_input4) { "11:30:10" }
  let(:wrong_input1) { "000:00:10" }
  let(:wrong_input2) { "00:020:10" }
  let(:wrong_input3) { "01:20:100" }
  let(:wrong_input4) { "11:30:1" }
  describe '#parse' do
    context "when valid_input1" do
      before do
        @duration = DurationParser.new(valid_input1)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 0 }
      it { expect(@duration.minute).to eq 0 }
      it { expect(@duration.second).to eq 10.second }
    end

    context "when valid_input2" do
      before do
        @duration = DurationParser.new(valid_input2)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 0 }
      it { expect(@duration.minute).to eq 20.minute }
      it { expect(@duration.second).to eq 10.second }
    end

    context "when valid_input3" do
      before do
        @duration = DurationParser.new(valid_input3)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 1.hour }
      it { expect(@duration.minute).to eq 20.minute }
      it { expect(@duration.second).to eq 10.second }
    end

    context "when valid_input4" do
      before do
        @duration = DurationParser.new(valid_input4)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 11.hour }
      it { expect(@duration.minute).to eq 30.minute }
      it { expect(@duration.second).to eq 10.second }
    end

    context "when wrong_input1" do
      before do
        @duration = DurationParser.new(wrong_input1)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 0 }
      it { expect(@duration.minute).to eq 0 }
      it { expect(@duration.second).to eq 0 }
    end

    context "when wrong_input2" do
      before do
        @duration = DurationParser.new(wrong_input2)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 0 }
      it { expect(@duration.minute).to eq 0 }
      it { expect(@duration.second).to eq 0 }
    end

    context "when wrong_input3" do
      before do
        @duration = DurationParser.new(wrong_input3)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 0 }
      it { expect(@duration.minute).to eq 0 }
      it { expect(@duration.second).to eq 0 }
    end

    context "when wrong_input4" do
      before do
        @duration = DurationParser.new(wrong_input4)
        @duration.parse
      end
      it { expect(@duration.hour).to eq 0 }
      it { expect(@duration.minute).to eq 0 }
      it { expect(@duration.second).to eq 0 }
    end
  end
end
