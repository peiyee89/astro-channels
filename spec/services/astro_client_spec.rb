require 'rails_helper'

RSpec.describe AstroClient, vcr: true do
  describe '.get_channel_list' do
    before do
      @response = AstroClient.get_channel_list
    end

    it "return list of channel ids" do
      aggregate_failures "testing channel list response" do
        expect(@response.blank?).to be_falsey
        expect(@response.count).to be > 0
      end
    end
  end
end
