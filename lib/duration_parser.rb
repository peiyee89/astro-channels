class DurationParser
  attr_reader :hour, :minute, :second
  def initialize(duration)
    @duration = duration
  end

  def parse
    captures = @duration.split(":").delete_if { |c| c.size != 2 }
    @invalid_format = captures.size != 3
    @hour, @minute, @second = captures.map(&:to_i)
  end

  def hour
    return 0 if @invalid_format
    @hour.hour
  end

  def minute
    return 0 if @invalid_format
    @minute.minute
  end

  def second
    return 0 if @invalid_format
    @second.second
  end
end
