module UrlUtility
  def query_string_builder(params)
    return "" if params.blank?
    "?" + params.map { |key, value| "#{key}=#{value}" }.join("&")
  end
end
