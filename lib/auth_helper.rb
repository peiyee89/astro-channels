class AuthHelper
  attr_reader :auth
  def initialize(auth)
    @auth = auth
  end

  def provider
    auth.provider
  end

  def uid
    auth.uid
  end

  def access_token
    auth.credentials.token
  end

  def name
    auth.info.name
  end

  def email
    auth.info.email
  end
end
