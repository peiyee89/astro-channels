module AstroClient
  extend UrlUtility
  DEFAUL_SORT_ATTRIBUTE = "channelTitle".freeze
  BASE_URL = "http://ams-api.astro.com.my/".freeze
  GET_CHANNEL_LIST_URL = BASE_URL + "ams/v3/getChannelList".freeze
  GET_CHANNELS_URL = BASE_URL + "ams/v3/getChannels".freeze
  GET_EVENTS_URL = BASE_URL + "ams/v3/getEvents".freeze

  def self.get_channel_list
    response = HTTParty.get(GET_CHANNEL_LIST_URL)
    response["channels"].sort_by { |ch| ch[DEFAUL_SORT_ATTRIBUTE] } if response.code == 200
  end

  def self.get_channels(**params)
    order_by = params.delete(:order_by) || DEFAUL_SORT_ATTRIBUTE
    response = HTTParty.get(GET_CHANNELS_URL + query_string_builder(params))
    response["channel"].sort_by { |ch| ch[order_by] } if response.code == 200
  end

  def self.get_events(**params)
    response = HTTParty.get(GET_EVENTS_URL + query_string_builder(params))
    response["getevent"].sort_by { |event| event[DEFAUL_SORT_ATTRIBUTE] } if response.code == 200
  end
end
