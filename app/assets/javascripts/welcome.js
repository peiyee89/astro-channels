$(document).on('turbolinks:load', function () {
  markUserFaveChannel();
  $('#sort-by-channel-name').on("click", function (event) {
    var sortAttr = 'channel-name';
    $('.channel-each-card').sort(function(a,b) {
       return $(a).data(sortAttr) > $(b).data(sortAttr) ? 1 : -1;;
    }).appendTo('.list-unstyled.row');
    event.preventDefault();
  });

  $('#sort-by-channel-number').on("click", function (event) {
    var sortAttr = 'channel-number';
    $('.channel-each-card').sort(function(a,b) {
       return $(a).data(sortAttr) > $(b).data(sortAttr) ? 1 : -1;;
    }).appendTo('.list-unstyled.row');
    event.preventDefault();
  });
});

function markUserFaveChannel(){
  var list = $('#channel-list').data('user-faved-list');
  if(list != null && list.length > 0) {
    list.forEach(function(element){
      $('#fave_' + element).css({fill: 'red'});
    })
  }
}
