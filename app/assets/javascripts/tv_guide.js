(function() {
  var loading = false;

  this.tvGuideInfiniteScroll = (function() {
    function tvGuideInfiniteScroll() {}

    tvGuideInfiniteScroll.setup = function() {
      if ($(".load-more-container").length == 0) {
        return;
      }

      var options = {
        distance: 50,
        callback: function(done) {
          if (loading) {
            done();
            return;
          }
          loading = true;
          var container = $(".load-more-container");
          var current_page = container.attr("data-page");
          console.log("current_page: ", current_page);
          var next_page = current_page*1 + 1;

          $.get("/next_page?page="+next_page, function(data, status) {
              if (status == "success") {
                if(data === ""){
                  $(".section-loading").hide();
                }
                console.log("next_page", next_page);
                container.append(data);
                container.attr("data-page", next_page);
              }
              loading = false;
          });

          done();
        }
      }
      // setup infinite scroll
      infiniteScroll(options);
    }

    return tvGuideInfiniteScroll;
  })();

}).call(this);
$(document).ready(function(){
  tvGuideInfiniteScroll.setup();
});
