class ChannelEventPresenter < ChannelPresenter
  def programme_title
    channel["programmeTitle"]
  end

  def display_time
    Time.parse(channel["displayDateTime"]).strftime("%I:%m %p")
  end
end
