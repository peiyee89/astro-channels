class ChannelPresenter
  attr_reader :channel
  def self.from_list(*channels)
    channels.flatten.map { |ch| ChannelPresenter.new(ch) }
  end

  def initialize(channel)
    @channel = channel
  end

  def channel_id
    channel["channelId"]
  end

  def channel_title
    channel["channelTitle"]
  end

  def channel_number
    "CH " + channel["channelStbNumber"]
  end

  def channel_pos_logo_small
    logo = channel["channelExtRef"].detect { |ext| ext["system"] == "Logo" && ext["subSystem"] =~ /Pos_300x185/ }
    logo["value"]
  end
end
