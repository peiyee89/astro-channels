class SessionsController < ApplicationController
  def create
    user = User.find_by(email: session_params[:email])
    if user && user.authenticate(session_params[:password])
      self.current_user = user
      redirect_to root_path
    else
      flash[:notice] = "Looks like you have enter wrong email or password."
      redirect_to new_session_path
    end
  end

  def destroy
    self.current_user = nil
    redirect_to root_path
  end

  protected

  def session_params
    params.require(:session).permit(:email, :password)
  end
end
