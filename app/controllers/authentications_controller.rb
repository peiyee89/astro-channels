class AuthenticationsController < ApplicationController
  def create
    @auth = AuthHelper.new(request.env["omniauth.auth"])
    if authenticated_user?
      render nothing: true
      return
    elsif current_user
      current_user.authentications.create(authentication_params)
    else
      authenticated = Authentication.find_by(provider: @auth.provider, uid: @auth.uid)
      if authenticated
        self.current_user = authenticated.user
      else
        user = User.create(email: @auth.email, name: @auth.name)
        user.authentications.create(authentication_params)
        self.current_user = user
      end
    end
    redirect_to root_path
  end

  private

  def authentication_params
    {
      provider: @auth.provider,
      access_token: @auth.access_token,
      uid: @auth.uid,
    }
  end

  def authenticated_user?
    current_user && current_user.authentications.where(provider: @auth.provider, uid: @auth.uid).present?
  end
end
