module ChannelTimeEventHelper
  extend ActiveSupport::Concern

  included do
    API_TIME_FORMAT = "%Y-%m-%d %H:%M".freeze
    def period_start
      DateTime.now.in_time_zone(Time.zone).beginning_of_day.strftime(API_TIME_FORMAT)
    end

    def period_end
      (DateTime.now.in_time_zone(Time.zone) + 4.hours).strftime(API_TIME_FORMAT)
    end

    def not_current_or_future_show?(display_datetime, duration)
      current_time = DateTime.now.in_time_zone(Time.zone)
      display_datetime = ActiveSupport::TimeZone[Time.zone.name].parse(display_datetime)
      return false if display_datetime > current_time
      duration = DurationParser.new(duration)
      duration.parse
      (display_datetime + duration.hour + duration.minute + duration.second) < current_time
    end
  end
end
