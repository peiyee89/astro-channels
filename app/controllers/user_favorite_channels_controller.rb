class UserFavoriteChannelsController < ApplicationController
  def create
    if current_user
      @channel_id = params[:channel_id]
      faved = current_user.user_favorite_channels.find_by(channel_id: @channel_id)
      @color = '#FFF'
      if faved
        faved.destroy
      else
        current_user.user_favorite_channels.create(channel_id: @channel_id)
        @color = '#FF0000'
      end
      respond_to do |format|
        format.js
      end
    else
      redirect_to new_session_path
    end
  end
end
