class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      self.current_user = @user
      render(js: "Turbolinks.visit('#{root_path}');")
    else
      flash[:notice] = "Looks like you have already claimed your account. Please log in to continue."
      render(js: "Turbolinks.visit('#{new_session_path}');")
    end
  end

  protected

  def user_params
    params.require(:user).permit(:email, :password, :name)
  end
end
