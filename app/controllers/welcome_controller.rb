class WelcomeController < ApplicationController
  PER_PAGE = 20
  include ChannelTimeEventHelper
  before_action :get_user_faved_list, only: [:index, :tv_guide]

  def index
    channels = AstroClient.get_channels(order_by: params[:order_by])
    @user_faved_list = current_user ? current_user.user_favorite_channels.pluck(:channel_id).uniq : []
    @channel_presenters = ChannelPresenter.from_list(channels)
  end

  def tv_guide
    page = params[:page].to_i
    @channel_event_groups = get_channel_event_groups(page)
  end

  def next_page
    page = params[:page].to_i
    @channel_event_groups = get_channel_event_groups(page)
    render layout: false
  end

  protected

  def get_channel_event_groups(page)
    index_start = page * PER_PAGE
    index_end = (page + 1) * PER_PAGE - 1
    index_end = index_end > channel_ids.size ? channel_ids.size : index_end
    events = AstroClient.get_events(channelId: channel_ids[index_start..index_end],
                                    periodStart: period_start,
                                    periodEnd: period_end)
    groups = events.group_by { |event| event["channelId"] }
    groups.each_value do |value|
      value.delete_if { |ch| not_current_or_future_show?(ch["displayDateTime"], ch["displayDuration"]) }
      value.sort_by! { |ch| ch["displayDateTime"] }
      value.map! { |ch| ChannelEventPresenter.new(ch) }
    end
    groups
  end

  def channel_ids
    @channel_ids ||= AstroClient.get_channel_list.map { |ch| ch["channelId"] }
  end
end
