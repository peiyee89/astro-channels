module ApplicationHelper
  def body_class
    "#{controller_name} #{action_name}"
  end

  def enable_turbolink
    controller_name != "users" && controller_name != "sessions"
  end
end
