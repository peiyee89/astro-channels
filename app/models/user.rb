class User < ActiveRecord::Base
  has_many :authentications
  has_many :user_favorite_channels
  validates :email, presence: true, email: true, allow_blank: false, uniqueness: true
  validates :name, presence: true, allow_blank: false
  has_secure_password validations: false
end
