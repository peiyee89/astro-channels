class Authentication < ActiveRecord::Base
  belongs_to :user
  validates :provider, presence: true, allow_blank: false
  validates :uid, presence: true, allow_blank: false
end
