class UserFavoriteChannel < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  validates :channel_id, presence: true, uniqueness: { scope: :user_id }
end
